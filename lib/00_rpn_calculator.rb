require 'byebug'
class RPNCalculator

  @@operations = {
    :+ => "plus",
    :- => "minus",
    :* => "times",
    :/ => "divide"
  }

  # @@operations = {
  #   :+ => Proc.new { plus.call },
  #   :- => Proc.new { minus.call },
  #   :* => Proc.new { times.call },
  #   :/ => Proc.new { divide.call }
  # }

  def initialize
    @stack = [0]
  end

  def push(num)
    @stack << num
  end

  def plus
    raise_error_if_empty
    @stack.push(@stack.pop + @stack.pop)
  end

  def minus
    raise_error_if_empty
    last_val = @stack.pop
    second_to_last_val = @stack.pop
    @stack.push(second_to_last_val - last_val)
  end

  def divide
    raise_error_if_empty
    last_val = @stack.pop
    second_to_last_val = @stack.pop
    @stack.push(second_to_last_val.to_f / last_val)
  end

  def times
    raise_error_if_empty
    @stack.push(@stack.pop * @stack.pop)
  end

  def value
    @stack.last
  end

  def tokens(string)
    string.split(" ").map do |n|
      if %w(1 2 3 4 5 6 7 8 9 0).include?(n)
        n.to_i
      elsif ["+", "-", "*", "/"].include?(n)
        n.to_sym
      end
    end
  end

  def evaluate(string)
    instructions = tokens(string)
    instructions.each do |n|
      case n
      when 0..9
        push(n)
      else
        eval(@@operations[n])
        # Trying to find an elegant way to do this without eval.
        # @@operations[n].call
      end
    end
    value
  end

  def raise_error_if_empty
    raise "calculator is empty" if @stack.length < 2
  end
end
